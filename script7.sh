read -p "Introduzca un día de la semana en formato numérico, por favor: " dia

while [ $dia -lt 1 ] || [ $dia -gt 30 ]; do
    read -p "Introduzca un día correcto, por favor: " dia
done

case $dia in
    1)
	echo "Lunes"
    ;;
    2)
	echo "Martes"
    ;;
    3)
	echo "Miércoles"
    ;;
    4)
	echo "Jueves"
    ;;
    5) 
	echo "Viernes"
    ;;
    6)
	echo "Sábado"
    ;;
    7)
	echo "Domingo"
    ;;
    8)
	echo "Lunes"
    ;;
    9) 
	echo "Martes"
    ;;
    10)
	echo "Miércoles"
    ;;
    11)
	echo "Jueves"
    ;;
    12)
	echo "Viernes"
    ;;
    13)
	echo "Sábado"
    ;;
    14)
	echo "Domingo"
    ;;
    15)
	echo "Lunes"
    ;;
    16)
	echo "Martes"
    ;;
    17)
	echo "Miércoles"
    ;;
    18)
	echo "Jueves"
    ;;
    19) 
	echo "Viernes"
    ;;
    20)
	echo "Sábado"
    ;;
    21)
	echo "Domingo"
    ;;
    22) 
	echo "Lunes"
    ;;
    23)
	echo "Martes"
    ;;
    24)
	echo "Miércoles"
    ;;
    25)
	echo "Jueves"
    ;;
    26)
	echo "Viernes"
    ;;
    27)
	echo "Sábado"
    ;;
    28)
	echo "Domingo"
    ;;
    29)
	echo "Lunes"
    ;;
    30)
	echo "Martes"
    ;;
esac