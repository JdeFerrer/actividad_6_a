read -p "Introduzca su nota, por favor: " nota
valor=0
valor1=5
valor3=6
valor4=8
valor5=10

while [ $valor -gt $nota ]; do
    read -p "Introduzca una nota válida, por favor: " nota
done

if [ $nota -lt $valor1 ]; then
    echo "Has suspendido"
elif [ $nota -ge $valor1 ] && [ $nota -lt $valor3 ]; then
    echo "Has aprobado"
elif [ $nota -ge $valor3 ] && [ $nota -lt $valor4 ]; then
    echo "Obtienes un Bien"
elif [ $nota -ge $valor4 ] && [  $nota -lt $valor5 ]; then
    echo "Obtienes un Notable"
else 
    echo "Obtienes un SOBRESALIENTE"
fi 