read -p "Introduzca un número superior a 0, por favor: " numero
valor=0 
resto=0

while [ $valor -ge $numero ]; do
    read -p"Introduzca un número superior a 0, por favor: " numero
done

resto=`expr $numero % 2`

if [ $resto -eq 0 ]; then

    echo "El número introducido es par"

else
    
    echo "El número introducido es impar"

fi
