read -p "Introduzca la cantidad total de litros de agua consumida, por favor: " litrosAgua

valor1=50
valor2=150
valorTotal=200
precio1=0.4
precio2=0.20
precio3=0.10
acumulador=20
acumulador2=30
ac2=0

if [ $valor1  -ge  $litrosAgua ]; then
    echo " "
    echo "El coste total en euros es  de:"
    echo "scale=2; $litrosAgua * $precio1" | bc 

elif [ $litrosAgua -gt $valor1 ] && [ $litrosAgua -le $valorTotal ]; then
   ac2=`expr $litrosAgua - $valor1`
    echo " "
    echo "El coste total en euros es de:"
    echo "scale=2; $ac2 * $precio2 + $acumulador" | bc

else
   ac2=`expr $litrosAgua - $valorTotal`
   echo " "
   echo "El coste total en euros es de:"
   echo "scale=2; $ac2 * $precio3 + $acumulador + $acumulador2" |bc
   

fi

    