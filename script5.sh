read -p "Introduzca un número, por favor: " numero
contador=0
valor=0
acumulador=0
sumatoria=1
valorMedio=0

while [ $numero -gt $valor ]; do
    contador=`expr $contador + 1`
    acumulador=`expr $acumulador + $numero`
    echo " "
    read -p "Introduzca un número, por favor, pero si no quiere introducir más números, introduzca 0: " numero
    echo " "
done
valorMedio=`expr $acumulador / $contador`
echo "Suma total de los valores introducidos: $acumulador"
echo "Valor medio de los valores introducidos: $valorMedio"