read -p "Introduzca un número superior a 0, por favor: " numero
valor=0

while [ $valor -ge $numero ]; do
    echo "Número erróneo"
    read -p "Introduzca un número superior a 0, por favor: " numero
done

for i in `seq $valor $numero`; do
    echo " "
    echo "El valor es $i de $numero"
done